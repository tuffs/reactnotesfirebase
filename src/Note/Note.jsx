import React, { Component } from 'react'
import './Note.css'
import PropTypes from 'prop-types'

// Note Component
class Note extends Component {
  constructor(props) {
    super(props)
    this.noteContent = props.noteContent
    this.noteId = props.noteId
    this.handleRemoveNote = this.handleRemoveNote.bind(this)
  }
  handleRemoveNote(id) {
    this.props.removeNote(id)
  }
  render(props){
    return(
      <div>
        <div className="note fade-in">
          <span className="closebtn" onClick={() => this.handleRemoveNote(this.noteId)}>
            &times;
          </span>
          <p className="noteContent">{this.noteContent}</p>
        </div>
      </div>
    )
  }
}

// Prop Binding
Note.propTypes = {
  noteContent: PropTypes.string
}

// Def Export
export default Note;
